import a
import c
import dir1.dir2.mod


print("This is module b.py")


def function_from_b():
    print("This is function from module b")
    a.function_from_a()
    c.function_from_c()
    dir1.dir2.mod.function_from_mod()


function_from_b()
