import dir1.dir2.mod

from dir1.dir2 import mod


print("This is module d.py")


def function_from_d():
    print("This is function from module d")
    mod.function_from_mod()
    dir1.dir2.mod.function_from_mod()


function_from_d()
