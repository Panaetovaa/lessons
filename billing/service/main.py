import logging
import logging.config

from fastapi import FastAPI

from service import settings
from service import routers
from service import db


def init_app():
    logging.config.dictConfig(settings.LOGGING)

    app = FastAPI(
        title='Billing service',
        debug=True,
        version='1.0.0',
        docs_url="/docs",
        redoc_url="/redoc",
    )

    app.include_router(routers.users.router, prefix="/users")

    app.on_event("shutdown")(_on_shutdown)
    app.on_event("startup")(_on_startup)
    return app


async def _on_startup():
    db.on_startup()


async def _on_shutdown():
    db.on_shutdown()


app = init_app()
