import logging
from typing import List

from fastapi import APIRouter
from fastapi import HTTPException
from pydantic import BaseModel

from service.models.users import User


logger = logging.getLogger(__name__)


router = APIRouter()


class CreateRequestModel(BaseModel):
    name: str
    country: str
    city: str


class CreateReplyModel(BaseModel):
    id: int


@router.post("/", response_model=CreateReplyModel)
async def view_create(data: CreateRequestModel):
    user = User.create(**data.dict())

    return {
        "id": user.id,
    }


class GetReplyModel(BaseModel):
    id: int
    name: str
    country: str
    city: str


@router.get("/", response_model=List[GetReplyModel])
async def view_all():
    users = User.all()

    return [
        user.to_dict()
        for user in users.values()
    ]


@router.get("/{user_id: int}", response_model=GetReplyModel)
async def view_get(user_id):
    user = User.get(int(user_id))

    if not user:
        raise HTTPException(status_code=404, detail="User not found")

    return user.to_dict()
