import os


STORAGE_HOST = os.getenv("SERVICE_STORAGE_HOST", "storage")
STORAGE_PORT = os.getenv("SERVICE_STORAGE_PORT", 8000)


LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'default': {
            'format': "[%(asctime)s] %(levelname)s [%(name)s:%(lineno)s] %(message)s",
            'datefmt': "%d/%b/%Y %H:%M:%S"
        },
    },
    'handlers': {
        'console': {
            'level': os.getenv('SERVICE_LOGGERS_HANDLERS_LEVEL', 'INFO'),
            'class': 'logging.StreamHandler',
            'formatter': 'default'
        },
    },
    'loggers': {
        '': {
            'handlers': ['console'],
            'level': os.getenv('SERVICE_LOGGERS_HANDLERS_LEVEL', 'INFO'),
        },
    }
}
