from service import db


class User:
    def __init__(self, id, name, country='', city=''):
        self.id = id
        self.name = name
        self.country = country
        self.city = city

    @classmethod
    def get(cls, id):
        registry = db.get()
        return registry.get('users', {}).get(id)

    @classmethod
    def all(cls):
        registry = db.get()
        return registry.get('users', {})

    def to_dict(self):
        return {
            "id": self.id,
            "name": self.name,
            "country": self.country,
            "city": self.city,
        }

    @classmethod
    def create(cls, **params):
        registry = db.get()
        registry.setdefault('users', {})
        next_id = cls.get_next_id()

        user = cls(
            id=next_id, **params,
        )
        registry['users'][next_id] = user
        return user

    @classmethod
    def get_next_id(cls):
        registry = db.get()
        users = registry.get('users', {})
        return len(users) + 1
