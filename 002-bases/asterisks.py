def kill(a, b, c, *pids):
    print(f"kill({a}, {b}, {c}, {pids})")
    print(f"I will kill processes with pids {pids}")  # -> []


kill('a', 'b', 'c', 1, 2, 3, 4)


def pay_salaries(d, e, **salaries):
    print(f"pay_salaries({d}, {e}, {salaries})")
    print(f"I will pay salaries: {salaries}")


pay_salaries(1, 2, vanya=100000, anton=20000)
