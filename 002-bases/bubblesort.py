def bubblesort_1(array):
    """ Сортировка пузырьком. Простейший пример работы с
        циклами и условиями.
    """
    from_index = 0
    to_index = len(array) - 1
    while True:
        is_swapped = False

        for index in range(from_index, to_index):
            if array[index] > array[index + 1]:
                swap(array, index, index + 1)
                is_swapped = True

        to_index -= 1
        if not is_swapped:
            break

    return array


def swap(array, index1, index2):
    # temp = array[index1]
    # array[index1] = array[index2]
    # array[index2] = temp
    array[index1], array[index2] = array[index2], array[index1]


print(bubblesort_1([2, 1, 3]))

try:
    # будет ошибка, потому что set не поддерживает получение
    # элемента по индексу.
    print(bubblesort_1({1, 2, 3}))
except TypeError:
    pass

try:
    # будет ошибка, потому что str - это неизменяемое значение
    print(bubblesort_1('asd'))
except TypeError:
    pass
