INPUT_FILE = "logs.txt"
OUTPUT_FILE = "output.txt"


def _filter_http2_requests():
    f = open(INPUT_FILE, "r")

    for line in f:
        if "HTTP/2.0" in line:
            yield line

    f.close()


def _get_http2_requests():
    f = open(INPUT_FILE, "r")

    found_lines = []
    for line in f:
        if "HTTP/2.0" in line:
            found_lines.append(line)

    f.close()
    return found_lines


def dump_http_requests():
    f = open(OUTPUT_FILE, "wb")
    for line in _get_http2_requests():
        f.write(line.encode('utf-8'))

    f.close()


def dump_http_requests_optimal():
    f = open(OUTPUT_FILE, "wb")
    for line in _filter_http2_requests():
        f.write(line.encode('utf-8'))

    f.close()


# dump_http_requests_optimal()
dump_http_requests()
