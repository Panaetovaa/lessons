def overlap(sequence1, sequence2):
    """ Найти пересечение двух множеств"""

    result = []

    for elem1 in sequence1:
        for elem2 in sequence2:
            if elem1 == elem2:
                result.append(elem1)

    # Возвращаем с повторами элементов и сложностью O(n * m).
    return result


print(overlap([1, 2, 3, 4], [2, 3, 4, 4, 5, 6]))  # -> [2, 3, 4, 4]


def overlap_on_one_set(sequence1, sequence2):
    """ Найти пересечение двух множеств более оптимально"""

    result = []

    sequence2_set = set(sequence2)

    for elem1 in sequence1:
        if elem1 in sequence2_set:
            result.append(elem1)

    # Часть повторов отбрасываем, сложность O((n + m) * Lg(m))
    return result


print(overlap_on_one_set([1, 2, 3, 4], [2, 3, 4, 4, 5, 6]))  # -> [2, 3, 4]


def overlap_on_sets(sequence1, sequence2):
    sequence1_set = set(sequence1)
    sequence2_set = set(sequence2)

    # O (n * Lg(n) + m * Lg(m) + n * Lg(m))
    return sequence1_set & sequence2_set


print(overlap_on_sets([1, 2, 3, 4], [2, 3, 4, 4, 5, 6]))  # {2, 3, 4}


def overlap_on_one_dict(sequence1, sequence2):
    """ Найти пересечение двух множеств более оптимально"""

    result = []

    sequence2_dict = {k: True for k in sequence2}

    for elem1 in sequence1:
        if elem1 in sequence2_dict:
            result.append(elem1)

    # O (n + m)
    return result


print(overlap_on_one_set([1, 2, 3, 4], [2, 3, 4, 4, 5, 6]))  # -> [2, 3, 4]


def super_overlap(min_sequences_qty, min_qty, *sequences):
    """ Найти элементы, которые есть в не менее чем min_sequences_qty
        последовательносей не менее чем min_qty раз.
    """

    # Dict:
    # {
    #     "значение элемента": {
    #         "номер множества": "число элементов в множестве",
    #         "номер множества": "число элементов в множестве",
    #         ...
    #     }
    # }

    quantities = {}

    for seq_index, seq in enumerate(sequences):
        for elem in seq:
            qty_of_elem = quantities.setdefault(elem, {})
            qty_of_elem.setdefault(seq_index, 0)
            qty_of_elem[seq_index] += 1

    result = []
    for elem, qty_of_elem in quantities.items():
        number_of_sequences = 0

        number_of_sequences = sum(
            1 if qty >= min_qty else 0 for qty in qty_of_elem.values()
        )
        if number_of_sequences >= min_sequences_qty:
            result.append(elem)

    return result


print(super_overlap(2, 2, [1, 2, 2, 3, 3, 3], [2, 2, 3, 3, 3]))
