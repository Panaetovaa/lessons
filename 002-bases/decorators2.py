import time
import random


_CACHE = {}


def cache(ttl=10):
    def decorator(func):
        def _func(symbol):
            cached = _CACHE.get(symbol)
            if not cached or time.time() - cached.get('updated_at', 0) > ttl:
                quote = func(symbol)
                _CACHE[symbol] = {
                    "quote": quote,
                    "updated_at": time.time(),
                }
            else:
                print(f"Getting quote {symbol} from cache")

            return _CACHE[symbol]['quote']

        return _func

    return decorator


@cache(10)
def get_quote(symbol):
    print(f"Generate quote {symbol}")
    return random.random()


get_quote('EURUSD')
get_quote('EURJPY')
get_quote('EURUSD')
get_quote('EURUSD')
get_quote('EURUSD2')
