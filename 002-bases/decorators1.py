import time


def run_tests(*args, **kwargs):
    print("Running tests")
    time.sleep(5)
    print("Tests are done")


def upgrade_image_in_kuber(*args, **kwargs):
    print("Upgrade done")


def complicated_deploy(*args, **kwargs):
    print("Start complicated deploy")
    run_tests(*args, **kwargs)
    upgrade_image_in_kuber(*args, **kwargs)
    return "OK"


print("Let's deploy!")
complicated_deploy()
print("\n\n")


def crash():
    print("Crash happend! Panic!")


def manage_team(func):
    def _func(*args, **kwargs):
        if kwargs.get('need_huyak'):
            print("Huyak-huyak and in prod!")
            upgrade_image_in_kuber(*args, **kwargs)
            crash()
            return "OK"
        else:
            return func(*args, **kwargs)

    return _func


# Под капотом происходит следующее
# another_complicated_deploy = manage_team(another_complicated_deploy)
@manage_team
def another_complicated_deploy(*args, **kwargs):
    return complicated_deploy(*args, **kwargs)


print("Let's deploy!")
another_complicated_deploy(need_huyak=True)
