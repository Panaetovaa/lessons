#! /bin/bash

sudo add-apt-repository -y ppa:deadsnakes/ppa
sudo apt-get update
sudo apt-get install -y python3.8

sudo apt install -y python3.8-distutils

sudo python3.8 -m pip install --upgrade pip setuptools wheel

sudo rm /usr/bin/python || true
sudo ln -s /usr/bin/python3.8 /usr/bin/python

sudo rm /usr/bin/pip || true
sudo ln -s /usr/bin/pip3 /usr/bin/pip

sudo pip install --upgrade ipython
